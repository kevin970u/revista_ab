<?php
  class Revista extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("revista",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $revistas=$this->db->get("revista");
      if ($revistas->num_rows()>0) {
        return $revistas->result();
      } else {
        return false;
      }
    }


    public function consultarTodosConEditoriales() {
        $this->db->select('revista.*, editorial.nombre AS nombre_editorial');
        $this->db->from('revista');
        $this->db->join('editorial', 'revista.fkid_edi = editorial.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }


    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $revista = $this->db->get("revista");
    if ($revista->num_rows() > 0) {
        return $revista->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("revista");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("revista",$datos);
}




function obtenerListadoEditoriales()
   {
       $editoriales = $this->db->get("editorial")->result();
       return $editoriales;
   }


  }//Fin de la clase



?>
