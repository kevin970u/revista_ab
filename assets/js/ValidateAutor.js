
    $(document).ready(function() {
        $("#frm_nuevo_autor").validate({
            rules: {

                "nombre": {
                    required: true,
                    minlength: 5

                },

                "apellido": {
                  required: true,
                  minlength: 5
                },

                "direccion": {
                    required: true,
                    minlength: 5,
                    maxlength: 150

                },

                "pais": {
                    required: true,
                    minlength: 5,
                    maxlength: 150

                },

                "correo": {
                  required: true,
                  email: true

                },

                "telefono": {
                  required: true,
                  minlength: 10, // Puedes ajustar la longitud según tus necesidades
                  maxlength: 15,
                  pattern: /^\+?([0-9]{1,3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/

                }
            },
            messages: {

                "nombre": {
                    required: "Debe ingresar el nombre del autor",
                    minlength: "El nombre no debe tener menos de 5 caracteres"

                },
                "apellido": {
                  required: "Debe apellido el nombre del autor",
                  minlength: "El apellido no debe tener menos de 5 caracteres"

                },

                "direccion": {
                    required: "Debe ingresar la direccion del autor",
                    minlength: "Debe ingresar 5 o más caracteres",
                    maxlength: "solo se permite hasta 150 caracteres"

                },

                "pais": {
                  required: "Debe ingresar el pais de residencia",
                  minlength: "Debe ingresar 5 o más caracteres",
                  maxlength: "solo se permite hasta 150 caracteres"
                },

                "correo": {
                  required: "Por favor, ingresa tu correo electrónico",
                  email: "Por favor, ingresa un correo electrónico válido"
                },


                "telefono": {
                  required: "Por favor, ingresa tu número de teléfono",
                   minlength: "El número de teléfono debe tener al menos 10 dígitos",
                   maxlength: "El número de teléfono no puede tener más de 15 dígitos",
                   pattern: "Por favor, ingresa un número de teléfono válido"
                }

            }
        });
    });
