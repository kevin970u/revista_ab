<?php
    class Autor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("autor");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $autores=$this->db->get("autor");
      if ($autores->num_rows()>0) {
        return $autores->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $autor = $this->db->get("autor");
    if ($autor->num_rows() > 0) {
        return $autor->row();
    } else {
        return false;
    }
}

    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("autor");
    }

function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("autor",$datos);
}


  }//Fin de la clase
?>
