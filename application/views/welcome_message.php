
            <!-- Carousel Start -->
            <div class="header-carousel owl-carousel">
                <div class="header-carousel-item">
                    <img src="assets/img/1.jpg" class="img-fluid w-100" alt="Image">
                    <div class="carousel-caption">
                        <div class="carousel-caption-content p-3">
                            <h5 class="text-white text-uppercase fw-bold mb-4" style="letter-spacing: 3px;">Tecnología</h5>
                            <h1 class="display-1 text-capitalize text-white mb-4">Investigación Tecnológica</h1>
                            <p class="mb-5 fs-5">Transforma la realidad existente a través de la obtención de un conocimiento práctico con una metodología diferente.
                            </p>
                            <a class="btn btn-primary rounded-pill text-white py-3 px-5" href="#">Más información</a>
                        </div>
                    </div>
                </div>
                <div class="header-carousel-item">
                    <img src="assets/img/2.jpg" class="img-fluid w-100" alt="Image">
                    <div class="carousel-caption">
                        <div class="carousel-caption-content p-3">
                            <h5 class="text-white text-uppercase fw-bold mb-4" style="letter-spacing: 3px;">Gastronomía</h5>
                            <h1 class="display-1 text-capitalize text-white mb-4">Ciencias Gastronómicas</h1>
                            <p class="mb-5 fs-5 animated slideInDown">Innovar y diseñar deliciosos platos con nuevas texturas y sabores, que provoquen nuevas sensaciones en los clientes
                            </p>
                            <a class="btn btn-primary rounded-pill text-white py-3 px-5" href="#">Más información</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel End -->
        </div>
        <!-- Navbar & Hero End -->


        <!-- Services Start -->
        <div class="container-fluid service py-5">
            <div class="container py-5">
                <div class="section-title mb-5 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="sub-style">
                        <h4 class="sub-title px-3 mb-0">Sobre la Revista</h4>
                    </div>
                    <h1 class="display-3 mb-4">La revista académica y científica VICTEC</h1>
                    <p class="mb-0">Es una publicación semestral, perteneciente al Instituto Superior Tecnológico Vicente León de la ciudad de Latacunga, provincia de Cotopaxi, Ecuador, fundada en marzo del 2020.</p>
                </div>
                <div class="row g-4 justify-content-center">
                    <div class="col-md-6 col-lg-4 col-xl-3 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="service-item rounded">
                           <div class="service-img rounded-top">
                                <img src="assets/img/revista1.jpg" class="img-fluid rounded-top w-100" alt="">
                           </div>
                            <div class="service-content rounded-bottom bg-light p-4">
                                <div class="service-content-inner">
                                    <h5 class="mb-4">ACERCA DE LAS CONTRIBUCIONES</h5>
                                    <p class="mb-4">El Comité Editorial acepta tres (3) tipos de contribuciones para publicación en las distintas áreas de la Revista VICTEC...</p>
                                    <a href="#" class="btn btn-primary rounded-pill text-white py-2 px-4 mb-2">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xl-3 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="service-item rounded">
                           <div class="service-img rounded-top">
                                <img src="assets/img/revista2.jpg" class="img-fluid rounded-top w-100" alt="">
                           </div>
                            <div class="service-content rounded-bottom bg-light p-4">
                                <div class="service-content-inner">
                                    <h5 class="mb-4">ACERCA DE LOS ARTÍCULOS PARA PUBLICAR</h5>
                                    <p class="mb-4">Las contribuciones que se publiquen en VICTEC deben estar enmarcadas en los requisitos...</p>
                                    <a href="#" class="btn btn-primary rounded-pill text-white py-2 px-4 mb-2">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-xl-3 wow fadeInUp" data-wow-delay="0.5s">
                        <div class="service-item rounded">
                           <div class="service-img rounded-top">
                                <img src="assets/img/revista3.jpeg" class="img-fluid rounded-top w-100" alt="">
                           </div>
                            <div class="service-content rounded-bottom bg-light p-4">
                                <div class="service-content-inner">
                                    <h5 class="mb-4">POLÍTICA DE DERECHOS Y ACCESO ABIERTO</h5>
                                    <p class="mb-4">La revista VICTEC publica artículos originales e inéditos en la modalidad de acceso abierto, lo que significa que están...</p>
                                    <a href="#" class="btn btn-primary rounded-pill text-white py-2 px-4 mb-2">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-center wow fadeInUp" data-wow-delay="0.2s">
                        <a class="btn btn-primary rounded-pill text-white py-3 px-5" href="#">Más información</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services End -->


        <!-- About Start -->
        <div class="container-fluid about bg-light py-5">
            <div class="container py-5">
                <div class="row g-5 align-items-center">
                    <div class="col-lg-5 wow fadeInLeft" data-wow-delay="0.2s">
                        <div class="about-img pb-5 ps-5">
                            <img src="assets/img/publi1.jpg" class="img-fluid rounded w-100" style="object-fit: cover;" alt="Image">
                            <div class="about-img-inner">

                            </div>

                        </div>
                    </div>
                    <div class="col-lg-7 wow fadeInRight" data-wow-delay="0.4s">
                        <div class="section-title text-start mb-5">
                            <h4 class="sub-title pe-3 mb-0">EVALUACIÓN POR PARES</h4>
                            <h1 class="display-3 mb-4">Procedimiento para la selección y aprobación de artículos</h1>
                            <p class="mb-4">Los artículos inician el proceso de registro con la revisión de los requisitos solicitados: versión Word del artículo, Carta de originalidad y cesión de derechos, currículo abreviado de cada persona autora.
															La verificación del registro de artículos nuevos se realiza cada 2 semanas.</p>
                            <div class="mb-4">
                                <p class="text-secondary"><i class="fa fa-check text-primary me-2"></i> Chequeo para verificar que el escrito cumpla con los requisitos que se solicitan en las Instrucciones a autores/as.</p>
                                <p class="text-secondary"><i class="fa fa-check text-primary me-2"></i> Constatación de que el artículo no contenga plagio ni autoplagio y que atribuya de forma correcta.</p>
                                <p class="text-secondary"><i class="fa fa-check text-primary me-2"></i> El director asigna personas revisoras de acuerdo a la temática del artículo.</p>
                            </div>
                            <a href="#" class="btn btn-primary rounded-pill text-white py-3 px-5">Leer más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->

        <!-- Feature Start -->

        <!-- Feature End -->


        <!-- Book Appointment Start -->
        <div class="container-fluid appointment py-5">
            <div class="container py-5">
                <div class="row g-5 align-items-center">
                    <div class="col-lg-6 wow fadeInLeft" data-wow-delay="0.2">
                        <div class="section-title text-start">
                            <h4 class="sub-title pe-3 mb-0">Código de ética</h4>
                            <h1 class="display-4 mb-4">CÓDIGO DE ÉTICA</h1>
                            <p class="mb-4">VICTEC es una revista que busca la excelencia nacional e internacional, se inspira en el código del Comité de Ética de Publicaciones (COPE), dirigido tanto a editores como a revisores y autores. Así como las buenas prácticas en publicación y lo que no se debe hacer.</p>
                            <div class="row g-4">
                                <div class="col-sm-9">
                                    <div class="d-flex flex-column h-100">
                                        <div class="mb-4">
                                            <h5 class="mb-3"><i class="fa fa-check text-primary me-2"></i>Originalidad y fidelidad de los datos</h5>
                                            <p class="mb-0">Los autores de originales enviados a VICTEC mediante carta firmada donde atestiguan que el trabajo es original.</p>
                                        </div>
                                        <div class="mb-4">
                                            <h5 class="mb-3"><i class="fa fa-check text-primary me-2"></i> Atribucione, citas y referencias</h5>
                                            <p class="mb-0">El autor debe suministrar siempre la correcta
indicación de las fuentes y los aportes mencionados en el artículo. Aplique normas</p>
                                        </div>
                                        <div class="text-start mb-4">
                                            <a href="#" class="btn btn-primary rounded-pill text-white py-3 px-5">Más detalles</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 wow fadeInRight" data-wow-delay="0.4s">
                        <div class="appointment-form rounded p-5">
                            <p class="fs-4 text-uppercase text-primary">Registrarse</p>
                            <h1 class="display-5 mb-4">Perfil</h1>
                            <form>
                                <div class="row gy-3 gx-4">
                                    <div class="col-xl-6">
                                        <input type="text" class="form-control py-3 border-primary bg-transparent text-white" placeholder="Nombre">
                                    </div>
                                    <div class="col-xl-6">
                                        <input type="text" class="form-control py-3 border-primary bg-transparent text-white" placeholder="Apellido">
                                    </div>
                                    <div class="col-xl-6">
                                        <input type="text" class="form-control py-3 border-primary bg-transparent" placeholder="Afiliación">
                                    </div>
																		<div class="col-xl-6">
                                        <input type="text" class="form-control py-3 border-primary bg-transparent" placeholder="País">
                                    </div>
																		<div class="col-xl-6">
                                        <input type="email" class="form-control py-3 border-primary bg-transparent" placeholder="Correo Electrónico">
                                    </div>

                                    <div class="col-xl-6">
                                        <input type="date" class="form-control py-3 border-primary bg-transparent">
                                    </div>

                                    <div class="col-12">
                                        <textarea class="form-control border-primary bg-transparent text-white" name="text" id="area-text" cols="30" rows="5" placeholder="Escribe un comentario"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <button type="button" class="btn btn-primary text-white w-100 py-3 px-5">Enviar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Testimonial Start -->
        <div class="container-fluid testimonial py-5 wow zoomInDown" data-wow-delay="0.1s">
            <div class="container py-5">
                <div class="section-title mb-5">

                    <h1 class="display-3 mb-4">Artículos</h1>
                </div>
                <div class="testimonial-carousel owl-carousel">
                    <div class="testimonial-item">
                        <div class="testimonial-inner p-5">
                            <div class="testimonial-inner-img mb-4">
                                <img src="assets/img/lib2.jpg" class="img-fluid rounded-circle" alt="">
                            </div>
                            <p class="text-white fs-7">La capacitación docente como apoyo para la gamificación en la asignatura de Lengua y literatura
                            </p>
                            <div class="text-center">
                                <h5 class="mb-2">Juan Patricio Lema Chiluisa, Hipatia Soraya Proaño Alvarez, Gretel Vázquez Zubizarreta, Tatiana Tapia Bastidas</h5>


                            </div>
                        </div>
                    </div>
                    <div class="testimonial-item">
                        <div class="testimonial-inner p-5">
                            <div class="testimonial-inner-img mb-4">
                                <img src="assets/img/lib2.jpg" class="img-fluid rounded-circle" alt="">
                            </div>
                            <p class="text-white fs-7">Cualidades de candidatos que optan a un puesto de trabajo: una evaluación mediante análisis de correspondencias
                            </p>
                            <div class="text-center">
                                <h5 class="mb-2">Sara Mireya Moran Guerrero, Steven Rodrigo Mantilla Quezada, Fabian Martinez Ortiz</h5>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-item">
                        <div class="testimonial-inner p-5">
                            <div class="testimonial-inner-img mb-4">
                                <img src="assets/img/lib2.jpg" class="img-fluid rounded-circle" alt="">
                            </div>
                            <p class="text-white fs-7">Especias, hierbas aromáticas o condimentos usados en la cocina ecuatoriana
                            <div class="text-center">
                                <h5 class="mb-2">Diego Salazar Duque</h5>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonial End -->
