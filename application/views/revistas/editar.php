<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR REVISTAS
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('revistas/actualizarRevista'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_revista">

        <input type="hidden" value="<?php echo $revistaEditar->id; ?>" name="id" id="id">


        <label for="fkid_edi"><b>Editorial:</b></label>
      <select name="fkid_edi" id="fkid_edi" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith"  required>
          <option value="">--Seleccione la editorial--</option>
          <?php foreach ($listadoEditoriales as $editorial): ?>
              <?php if ($editorial->id == $revistaEditar->fkid_edi): ?>
                  <option value="<?php echo $editorial->id; ?>" selected><?php echo $editorial->nombre; ?></option>
              <?php else: ?>
                  <option value="<?php echo $editorial->id; ?>"><?php echo $editorial->nombre; ?></option>
              <?php endif; ?>
          <?php endforeach; ?>
      </select>
      <br>


        <div class="mb-3 text-dark">
            <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="<?php echo $revistaEditar->nombre; ?>" oninput="EspacioLetrasNumeros(this)" placeholder="Ingrese el nombre de la revista" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="fecha_publicacion" class="form-label"><b>Fecha publicación:</b></label>
            <input id="fecha_publicacion" type="date" name="fecha_publicacion" value="<?php echo $revistaEditar->fecha_publicacion; ?>"  placeholder="" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="issn" class="form-label"><b>Issn:</b></label>
            <input id="issn" type="text" name="issn" value="<?php echo $revistaEditar->issn; ?>"  placeholder="Ingrese el issn" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="volumen" class="form-label"><b>Volumen:</b></label>
            <input id="volumen" type="text" name="volumen" value="<?php echo $revistaEditar->volumen; ?>"  placeholder="Ingrese el volumen de la revista" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="numero" class="form-label"><b>Número:</b></label>
            <input id="numero" type="text" name="numero" value="<?php echo $revistaEditar->numero; ?>"  placeholder="Ingrese el numero de la revista" class="form-control" required>
        </div>



         <label for=""><b>Logo actual de la editorial:</b></label><br>
           <?php if (!empty($revistaEditar->logo)) : ?>
               <img src="<?php echo base_url('uploads/revistas/' . $revistaEditar->logo); ?>" alt="Imagen de la agencia" width="200"><br>
           <?php else: ?>
               <p>No hay logo disponible</p>
           <?php endif; ?>
        <br>
         <label for=""><b>Nuevo logo de la editorial:</b></label><br>

         <input type="file" name="fotografia_nueva" id="fotografia_nueva" accept="image/*" class="form-control">

        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('editoriales/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>
